<?php

/**
 * @file
 * Contains \Drupal\Core\Cache\CacheTagHandlerInterface
 */

namespace Drupal\Core\Cache;

/**
 * Defines required methods for classes wanting to handle cache tag changes.
 */
interface CacheTagHandlerInterface {

  /**
   * Marks cache items with any of the specified tags as invalid.
   *
   * @param string[] $tags
   *   The list of tags for which to invalidate cache items.
   */
  public function invalidateTags($tags);

  /**
   * Deletes cache items with any of the specified tags.
   *
   * @param string[] $tags
   *   The list of tags for which to delete cache items.
   */
  public function deleteTags($tags);

}
