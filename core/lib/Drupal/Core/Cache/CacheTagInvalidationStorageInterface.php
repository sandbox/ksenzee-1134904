<?php

/**
 * @file
 * Contains \Drupal\Core\Cache\CacheTagInvalidationStorageInterface.
 */

namespace Drupal\Core\Cache;

/**
 * An interface defining cache tag invalidation classes with storage.
 */
interface CacheTagInvalidationStorageInterface {

  /**
   * Returns cache tag invalidations.
   *
   * @return array
   */
  public function getTagInvalidations();

  /**
   * Returns cache tag deletions.
   *
   * @return array
   */
  public function getTagDeletions();
}
