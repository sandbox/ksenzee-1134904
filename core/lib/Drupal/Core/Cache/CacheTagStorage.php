<?php

/**
 * @file
 * Contains \Drupal\Core\Cache\CacheTagStorage.
 */

namespace Drupal\Core\Cache;

use \Drupal\Core\Database\Connection;

/**
 * Storage for cache tag invalidations and deletions.
 */
class CacheTagStorage implements CacheTagInvalidationStorageInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a DatabaseBackend object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * Stores cache tag invalidations.
   *
   * @param string[] $tags
   *   The list of tags for which to store invalidations.
   */
  public function invalidateTags(array $tags) {
    try {
      $tag_cache = &drupal_static('Drupal\Core\Cache\CacheTagStorage::tagCache', array());
      $invalidated_tags = &drupal_static('Drupal\Core\Cache\CacheTagStorage::invalidatedTags', array());
      foreach ($tags as $tag) {
        // Only invalidate tags once per request unless they are written again.
        if (isset($invalidated_tags[$tag])) {
          continue;
        }
        $invalidated_tags[$tag] = TRUE;
        unset($tag_cache[$tag]);
        $this->connection->merge('cachetags')
          ->insertFields(array('invalidations' => 1))
          ->expression('invalidations', 'invalidations + 1')
          ->key('tag', $tag)
          ->execute();
      }
    }
    catch (\Exception $e) {
      $this->catchException($e, 'cachetags');
    }
  }

  /**
   * Stores cache tag deletions.
   *
   * @param string[] $tags
   *   The list of tags for which to store deletions.
   */
  public function deleteTags(array $tags) {
    $tag_cache = &drupal_static('Drupal\Core\Cache\CacheTagStorage::tagCache', array());
    $deleted_tags = &drupal_static('Drupal\Core\Cache\CacheTagStorage::deletedTags', array());
    foreach ($tags as $tag) {
      // Only delete tags once per request unless they are written again.
      if (isset($deleted_tags[$tag])) {
        continue;
      }
      $deleted_tags[$tag] = TRUE;
      unset($tag_cache[$tag]);
      try {
        $this->connection->merge('cachetags')
          ->insertFields(array('deletions' => 1))
          ->expression('deletions', 'deletions + 1')
          ->key('tag', $tag)
          ->execute();
      }
      catch (\Exception $e) {
        $this->catchException($e);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getTagInvalidations() {

  }

  /**
   * {@inheritdoc}
   */
  public function getTagDeletions() {

  }

  /**
   * Act on an exception when cache might be stale.
   *
   * If the {cachetags} table does not yet exist, that's fine but if the table
   * exists and yet the query failed, then the cache is stale and the
   * exception needs to propagate.
   *
   * @param \Exception $e
   *   The exception.
   *
   * @throws \Exception
   */
  protected function catchException(\Exception $e) {
    if ($this->connection->schema()->tableExists('cachetags')) {
      throw $e;
    }
  }
}
