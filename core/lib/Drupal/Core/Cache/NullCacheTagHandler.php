<?php

/**
 * @file
 * Contains \Drupal\Core\Cache\NullCacheTagHandler.
 */

namespace Drupal\Core\Cache;

/**
 * A no-op implementation of CacheTagHandlerInterface, for the installer.
 */
class NullCacheTagHandler implements CacheTagHandlerInterface {

  /**
   * Holds an array of cache tag invalidators.
   *
   * @var CacheTagInvalidationInterface[]
   */
  protected $invalidators;

  public function invalidateTags($tags) {}

  public function deleteTags($tags) {}

  public function addHandler(CacheTagInvalidationInterface $service) {
    $this->invalidators[] = $service;
  }

}
