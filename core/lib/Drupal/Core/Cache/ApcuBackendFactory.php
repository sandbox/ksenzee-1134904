<?php

/**
 * @file
 * Contains \Drupal\Core\Cache\ApcuBackendFactory.
 */

namespace Drupal\Core\Cache;

use \Drupal\Component\Utility\Crypt;

class ApcuBackendFactory implements CacheFactoryInterface, CacheTagInvalidationInterface {

  /**
   * The site prefix string.
   *
   * @var string
   */
  protected $sitePrefix;

  /**
   * Constructs an ApcuBackendFactory object.
   */
  public function __construct() {
    $this->sitePrefix = Crypt::hashBase64(DRUPAL_ROOT . '/' . conf_path());
  }

  /**
   * Gets ApcuBackend for the specified cache bin.
   *
   * @param $bin
   *   The cache bin for which the object is created.
   *
   * @return \Drupal\Core\Cache\ApcuBackend
   *   The cache backend object for the specified cache bin.
   */
  public function get($bin) {
    return new ApcuBackend($bin, $this->sitePrefix);
  }

  /**
   * Deletes items with any of the specified tags.
   *
   * If the cache items are being deleted because they are no longer "fresh",
   * you may consider using invalidateTags() instead. This allows callers to
   * retrieve the invalid items by calling get() with $allow_invalid set to TRUE.
   * In some cases an invalid item may be acceptable rather than having to
   * rebuild the cache.
   *
   * @param array $tags
   *   Associative array of tags, in the same format that is passed to
   *   CacheBackendInterface::set().
   *
   * @see \Drupal\Core\Cache\CacheBackendInterface::set()
   * @see \Drupal\Core\Cache\CacheTagInvalidationInterface::invalidateTags()
   * @see \Drupal\Core\Cache\CacheBackendInterface::delete()
   * @see \Drupal\Core\Cache\CacheBackendInterface::deleteMultiple()
   * @see \Drupal\Core\Cache\CacheBackendInterface::deleteAll()
   */
  public function deleteTags(array $tags) {
    // TODO: Implement deleteTags() method.
  }

  /**
   * Marks cache items with any of the specified tags as invalid.
   *
   * @param array $tags
   *   Associative array of tags, in the same format that is passed to
   *   CacheBackendInterface::set().
   *
   * @see \Drupal\Core\Cache\CacheBackendInterface::set()
   * @see \Drupal\Core\Cache\CacheTagInvalidationInterface::deleteTags()
   * @see \Drupal\Core\Cache\CacheBackendInterface::invalidate()
   * @see \Drupal\Core\Cache\CacheBackendInterface::invalidateMultiple()
   * @see \Drupal\Core\Cache\CacheBackendInterface::invalidateAll()
   */
  public function invalidateTags(array $tags) {
    // TODO: Implement invalidateTags() method.
  }
}
