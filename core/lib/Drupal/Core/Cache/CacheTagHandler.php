<?php

/**
 * @file
 * Contains \Drupal\Core\Cache\CacheTagHandler.
 */

namespace Drupal\Core\Cache;

/**
 * Passes cache tag events to classes that wish to respond to them.
 */
class CacheTagHandler implements CacheTagHandlerInterface {

  /**
   * Holds an array of cache tag invalidators.
   *
   * @var CacheTagInvalidationInterface[]
   */
  protected $invalidators;

  public function invalidateTags($tags) {
    foreach ($this->invalidators as $invalidator) {
      $invalidator->invalidateTags($tags);
    }
  }

  public function deleteTags($tags) {
    foreach ($this->invalidators as $invalidator) {
      $invalidator->deleteTags($tags);
    }
  }

  public function addHandler(CacheTagInvalidationInterface $service) {
    $this->invalidators[] = $service;
  }

}
